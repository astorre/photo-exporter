import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VkFetchComponent } from './vk-fetch.component';

describe('VkFetchComponent', () => {
  let component: VkFetchComponent;
  let fixture: ComponentFixture<VkFetchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VkFetchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VkFetchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
