import { Component } from '@angular/core';
import { Photo } from '../photo/photo.model';

@Component({
  selector: 'app-vk-fetch',
  templateUrl: './vk-fetch.component.html',
  styleUrls: ['./vk-fetch.component.css']
})
export class VkFetchComponent {
  photos: Photo[];
  loading: boolean;

  constructor() { }

  updatePhotos(photos: Photo[]): void {
    this.photos = photos;
    // console.log("photos: ", this.photos);
  }

}
