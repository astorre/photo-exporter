import { Injectable, Inject } from '@angular/core';
import { Jsonp, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Photo } from '../photo/photo.model';

export const VK_API_URL = 'https://api.vk.com/method/photos.getAll';

@Injectable()
export class VkFetchService {

  constructor(private jsonp: Jsonp,
    @Inject(VK_API_URL) private apiUrl: string) {
  }

  fetch(accessToken: string): Observable<Photo[]> {
    const params: string = [
      `owner_id=-121950041`,
      `extended=1`,
      `photo_sizes=0`,
      `no_service_albums=0`,
      `need_hidden=0`,
      `skip_hidden=10`,
      `v=5.67`,
      `access_token=${accessToken}`,
      `callback=JSONP_CALLBACK`
    ].join('&');
    const queryUrl = `${this.apiUrl}?${params}`;
    // console.log(queryUrl);

    return this.jsonp.get(queryUrl)
      .map((response: Response) => {
        return (<any>response.json()).response.items.map(item => {
          // console.log("raw item", item);
          return new Photo({
            id: item.id,
            owner_id: item.owner_id,
            date: item.date,
            text: item.text,
            photo_130: item.photo_130,
            photo_1280: item.photo_1280
          });
        });
      });
  }
}
