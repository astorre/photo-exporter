import {
  VkFetchService,
  VK_API_URL
} from './vk-fetch.service';

export const vkFetchInjectables: Array<any> = [
  {provide: VkFetchService, useClass: VkFetchService},
  {provide: VK_API_URL, useValue: VK_API_URL}
];
