import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { JsonpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { vkFetchInjectables } from './vk-fetch/vk-fetch.injectables';
import { photoInjectables } from './photo/photo.injectables';

import { AppComponent } from './app.component';
import { PhotoComponent } from './photo/photo.component';
import { VkFetchComponent } from './vk-fetch/vk-fetch.component';
import { FetchBoxComponent } from './fetch-box/fetch-box.component';

import { NgHttpLoaderModule } from 'ng-http-loader/ng-http-loader.module';

@NgModule({
  declarations: [
    AppComponent,
    PhotoComponent,
    VkFetchComponent,
    FetchBoxComponent
  ],
  imports: [
    BrowserModule,
    JsonpModule,
    HttpClientModule,
    NgHttpLoaderModule
  ],
  providers: [
    vkFetchInjectables,
    photoInjectables
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
