import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpResponse, HttpEventType } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from "rxjs/Subject";
import { Photo } from '../photo/photo.model';

export const FLICKR_API_URL = 'https://photo-back.herokuapp.com/api/v1/flickr';

@Injectable()
export class PhotoService {

  constructor(private http: HttpClient,
    @Inject(FLICKR_API_URL) private apiUrl: string) {
  }

  post(photo: Photo): Observable<number> {

    var subject = new Subject<number>()
    const req = new HttpRequest('POST', this.apiUrl, {
      url: photo.photo_1280,
    },
    {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded'),
      reportProgress: true,
    });

    this.http.request(req).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        const percentDone = Math.round(100 * event.loaded / event.total);
        subject.next(percentDone);
      } else if (event instanceof HttpResponse) {
        subject.complete();
      }
    });

    return subject.asObservable();
  }
}
