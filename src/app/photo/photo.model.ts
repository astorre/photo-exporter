export class Photo {
  id: number;
  owner_id: number;
  date: number;
  text: string;
  photo_130: string;
  photo_1280: string;
  url: string;

  constructor(obj?: any) {
    this.id = obj && obj.id || null;
    this.owner_id = obj && obj.owner_id || null;
    this.date = obj && obj.date || null;
    this.text = obj && obj.text || null;
    this.photo_130 = obj && obj.photo_130 || null;
    this.photo_1280 = obj && obj.photo_1280 || null;
    this.url = `https://vk.com/albums${this.owner_id}?z=photo${this.owner_id}_${this.id}%2Fphotos${this.owner_id}`;
  }
}
