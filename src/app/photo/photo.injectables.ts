import {
  PhotoService,
  FLICKR_API_URL
} from './photo.service';

export const photoInjectables: Array<any> = [
  {provide: PhotoService, useClass: PhotoService},
  {provide: FLICKR_API_URL, useValue: FLICKR_API_URL}
];
