import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Photo } from './photo.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PhotoService } from '../photo/photo.service';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css']
})
export class PhotoComponent implements OnInit {

  @Output() uploading: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() photo: Photo;

  constructor(private http: HttpClient, private flickr: PhotoService) {}

  ngOnInit(): void {
    // this.uploading.emit(false);
  }

  sendToFlickr(): void {
    this.uploading.emit(false);

    this.flickr
      .post(this.photo)
      .subscribe(
        data => {
          console.log(data);
        },
        err => {
          console.log("Error occured");
        }
      );
  }

}
