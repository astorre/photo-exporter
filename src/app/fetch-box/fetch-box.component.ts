import {
  Component,
  OnInit,
  Output,
  EventEmitter
} from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/switch';

import { VkFetchService } from '../vk-fetch/vk-fetch.service';
import { Photo } from '../photo/photo.model';

@Component({
  selector: 'app-fetch-box',
  templateUrl: './fetch-box.component.html',
  styleUrls: ['./fetch-box.component.css']
})
export class FetchBoxComponent implements OnInit {
  private accessToken: string;

  @Output() loading: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() photos: EventEmitter<Photo[]> = new EventEmitter<Photo[]>();
  authUrl: string = "https://oauth.vk.com/authorize?client_id=6105585&display=page&redirect_uri=https://photo-back.herokuapp.com/api/v1/vk_auth_redirect&scope=photos&response_type=code&v=5.68&state=phexco";

  constructor(private vk: VkFetchService,
              private http: HttpClient) {}

  ngOnInit(): void {
    this.http.get('https://photo-back.herokuapp.com/api/v1/vk_token').subscribe(data => {
      // Read the result field from the JSON response.
      // console.log(data['token']);
      this.accessToken = data['token'];
    });

    Observable.fromEvent(document.querySelector('#req-btn'), 'click')
          .debounceTime(250)
          .do(() => this.loading.emit(true))
          .map(() => this.vk.fetch(this.accessToken))
          .switch()
          .subscribe(
            (photos: Photo[]) => {
              this.loading.emit(false);
              this.photos.emit(photos);
            },
            (err: any) => {
              console.log(`Error: ${err}`);
              this.loading.emit(false);
            },
            () => {
              this.loading.emit(false);
            }
          );
  }
}
