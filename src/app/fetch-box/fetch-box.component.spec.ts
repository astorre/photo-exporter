import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FetchBoxComponent } from './fetch-box.component';

describe('FetchBoxComponent', () => {
  let component: FetchBoxComponent;
  let fixture: ComponentFixture<FetchBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FetchBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FetchBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
