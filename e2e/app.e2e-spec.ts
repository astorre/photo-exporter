import { PhotoExporterPage } from './app.po';

describe('photo-exporter App', () => {
  let page: PhotoExporterPage;

  beforeEach(() => {
    page = new PhotoExporterPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
